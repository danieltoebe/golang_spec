# General Rules and Guidelines for working with Go at GPS Insight

## Use the Current Stable Release of Go

- Go releases a major version roughly every 6 months
- The Go team works very hard to ensure that new major releases are backwards compatible with previous versions

### Updating to the new version of Go

Go version map: 1.\<major\>.<minor/security>

- If you notice that there is a new version of Go, major release, or minor please publish it in the #gpsi_gophers and/or #development Slack channel(s)
- Make sure your personal and any build environments are updated, such as CI/CD, and you are running the same version
- Go through and build your projects on your local/CI environments to ensure there are no issues.
- Use the `go fix` tool to ensure there are no immediate issues: (Go Fix)[https://golang.org/cmd/fix/]
- Any minor/security updates are especially important.
- Always loudly communicate any of these changes to the members of the team

---
## Tools

### Editors

While you can use any editor you prefer, you should make sure to set it up for Go development.

Some recommendations:

- Make sure to gofmt on save
- At least use the `go vet` tool: (Vet)[https://golang.org/cmd/vet/]

### Go Tools

This is not an exhaustive list of tools but a general recommendation of tools not included in the Go distribution.
_please feel free to expand on this list and request tools to be added_

#### goimports

URL: https://godoc.org/golang.org/x/tools/cmd/goimports
What It Does: replaces `go fmt`, and also will organize, add missing, and remove unused imports

#### gometalinter

URL: https://github.com/alecthomas/gometalinter
What It Does: collects a bunch of linter tools together into a single command. Most editors will have a way to configure the linters used. Please note that using the default configuration will slow any editor

#### goconvey

URL: https://github.com/smartystreets/goconvey
What It Does: while this is a testing framework, we do not currently use, it has a great browser based GUI for running go tests in the background. If used will run tests on every file save. Does not integrate into editors.

---
## Structures

### $GOPATH Structure

Docs: https://golang.org/doc/code.html#GOPATH

While in the next year (2019), There might be some changes to how $GOPATH works, here is the recommended structure
```
~/go/ -- $GOPATH
  |- bin/ -- $GOBIN compiled installed binaries go here: reccomend to put it in your $PATH
  \_ src/ -- this is where all your local non vendored package source code goes
```

You can place your $GOPATH directory anywhere on your system. Go by default looks for it in $HOME/go.
- To ensure your Go environment variable are properly set use: `go env`

### Project Structure

Docs: https://github.com/golang-standards/project-layout

We have chosen to use the golang-standards project layout. We do not adhere strictly to this see table below See the link for a more detailed overview.

**Here is a minimal layout:**

| Path | Description | Notes |
| ---- | ----------- | ----- |
| ./cmd/ | where the directories of the binary source code goes |
| ./cmd/binary-name/ | directory where the binary source code goes | by default the with `go build` command the name will be inherited from the directory name |
| ./assets/ | where any assets like images are held here |
| ./config/ | all config file templates |
| ./examples/ | examples of how to run / use your code |
| ./test/ | any test related files go here | any filename_test.go files go next to their corresponding file |
| ./internal/ | any packages that should not be accessible to the outside world |
| ./vendor/ | dependency managed directory of your 3rd party libs |
| ./pkg | packages that can be used by 3rd party apps | Most likely will not be used because of the `gps_packages` repo
| ./Gopkg.lock | dep generated lock file for 3rd party libs | DO NOT EDIT |
| ./Gopkg.toml | dep generated file to help handle dependencies | editable |
| ./README.md | describes the service |
| ./Dockerfile | the Dockerfile for the RC |

### File Structure

**The heirarchy of elements in a go file are like this:**

- copyright block and global doc comments
- package declaration
- imports
- global const
- global vars
- init func
- type declaration
- New functions to create the type above
- methods belonging to the declared type above
- normal functions

**Here is an example**

```Go
/* Copyright block */

/* global docs */

package main

import (
    ...
)

const (
    ...
)

var (
    ...
)

func init() {}

type Something struct {
    ...
}

func NewSomething() *Something {}

func (s *Something) Method() {}

type SomethingElse {
    ...
}

....

func RegularFunc() {}

```

### Tests Files

- The unit test file name should be file the unit resides in underscore test dot go
- The name of the unit test function should be camelcase, start with and uppercase Test then the name of the unit. Optionally if there are multiple tests on a single unit then append underscore variance

**Here is an example:**

```Go
/* copyright block */

package something

import (...)

func TestSomething(t *testing.T) {}

func TestSomething_fail(t *esting.T) {}

```

---
